import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
  FormArray
} from "@angular/forms";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"]
})
export class FormComponent implements OnInit {
  companydata;
  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initializeCompanyForm("Purelogics", "Lahore", [
      this.createEmployee({ name: "Ali Uzair", phone: "1234567" })
    ]);
  }

  initializeCompanyForm(name = "", address = "", employees = []) {
    this.companydata = this.fb.group({
      name: [name, Validators.required],
      address: [address, Validators.required],
      employees: this.fb.array(employees)
    });
  }

  createEmployee({ name, phone } = { name: "", phone: "" }) {
    return this.fb.group({
      name: [name, Validators.required],
      phone: [phone, Validators.required]
    });
  }

  addNewEmployee() {
    const employees = this.companydata.get("employees") as FormArray;
    employees.push(this.createEmployee());
  }

  onSubmit() {
    localStorage.setItem(
      "localCompanyData",
      JSON.stringify(this.companydata.value)
    );
    // empty existing form
    this.initializeCompanyForm("Purelogics", "Lahore", [
      this.createEmployee({ name: "Ali Uzair", phone: "1234567" })
    ]);
    alert("Value Saved");
  }

  reloadData() {
    // fetch data from localstorage
    let localCompanyData;
    try {
      localCompanyData = JSON.parse(localStorage.getItem("localCompanyData"));
      if (!("employees" in localCompanyData)) {
        throw new Error("error");
      }
    } catch (error) {
      return alert("Received invalid data.");
    }

    // empty existing form
    this.initializeCompanyForm();

    // add empty employees according to received data
    const employees = this.companydata.get("employees") as FormArray;
    localCompanyData["employees"].forEach((e, i) => {
      this.addNewEmployee();
    });

    // patch values
    this.companydata.patchValue(localCompanyData);
  }

  removeEmployee(index, silent = false) {
    const employees = this.companydata.get("employees") as FormArray;
    if (silent || confirm("Are you sure?")) {
      employees.removeAt(index);
    }
  }
}
